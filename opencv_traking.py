import cv2

class ImageSubscriber(Node):
    iteration = 0
    center = [0, 0]

    def __init__(self):
        super().__init__('image_subscriber')
        self.cv_bridge = CvBridge()
        self.subscription = self.create_subscription(
            Image,
            '/world/iris_runway/model/camera/link/link/sensor/camera/image',
            self.image_callback,
            10)
        print('init ImageSubscriber')
        self.subscription
        tracker_types = ['BOOSTING', 'MIL', 'KCF', 'TLD', 'MEDIANFLOW', 'CSRT', 'MOSSE', 'GOTURN']
        self.tracker_type = tracker_types[5]
        self.bbox = (287, 23, 86, 320)

        if int(minor_ver) < 3:
            self.tracker = cv2.Tracker_create(self.tracker_type)
        else:
            if self.tracker_type == 'BOOSTING':
                self.tracker = cv2.legacy.TrackerBoosting_create()
            if self.tracker_type == 'MIL':
                self.tracker = cv2.legacy.TrackerMIL_create()
            if self.tracker_type == 'KCF':
                self.tracker = cv2.legacy.TrackerKCF_create()
            if self.tracker_type == 'TLD':
                self.tracker = cv2.legacy.TrackerTLD_create()
            if self.tracker_type == 'MEDIANFLOW':
                self.tracker = cv2.legacy.TrackerMedianFlow_create()
            if self.tracker_type == 'CSRT':
                self.tracker = cv2.legacy.TrackerCSRT_create()
            if self.tracker_type == 'MOSSE':
                self.tracker = cv2.legacy.TrackerMOSSE_create()
            if self.tracker_type == 'GOTURN':
                self.tracker = cv2.TrackerGOTURN_create()

    def image_callback(self, msg):
        # Конвертувати повідомлення Image в OpenCV зображення
        cv_image = self.cv_bridge.imgmsg_to_cv2(msg, desired_encoding='bgr8')
        # print('new frame...')
        # Відобразити зображення (потрібен імпорт cv2)
        img_width = 720
        img_height = 480
        cv_image = cv2.resize(cv_image, (img_width, img_height))
        frame = cv_image
        if self.iteration == 0:
            self.tracker.init(frame, (316, 400, 80, 30))
            # bbox = cv2.selectROI(frame, False)
            # print('bbox', bbox)
            # # Initialize tracker with first frame and bounding box
            # ok = self.tracker.init(frame, bbox)
            self.iteration += 1
        # Start timer
        timer = cv2.getTickCount()
        # Update tracker
        ok, bbox = self.tracker.update(frame)

        self.center = get_bbox_center(bbox)
        # print('bbox: ', self.center)

        # Calculate Frames per second (FPS)
        fps = cv2.getTickFrequency() / (cv2.getTickCount() - timer);

        # Draw bounding box
        if ok:
            # Tracking success
            p1 = (int(bbox[0]), int(bbox[1]))
            p2 = (int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3]))
            cv2.rectangle(frame, p1, p2, (255, 0, 0), 2, 1)
        else:
            # Tracking failure
            cv2.putText(frame, "Tracking failure detected", (100, 80), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 0, 255), 2)
        # Display tracker type on frame
        cv2.putText(frame, self.tracker_type + " Tracker", (100, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50, 170, 50), 2);

        # Display FPS on frame
        cv2.putText(frame, "FPS : " + str(int(fps)), (100, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (50, 170, 50), 2);
        cv2.putText(frame, "Pitch : " + str(int(pitch_in_degree)), (100, 80), cv2.FONT_HERSHEY_SIMPLEX, 0.75,
                    (50, 170, 50), 2);

        img_center_x = int(img_width / 2)
        img_center_y = int(img_height / 2)
        cv2.rectangle(frame, (img_center_x - 10, img_center_y - 10), (img_center_x + 20, img_center_y + 20),
                      (30, 30, 255), 2, 1)

        cv2.imshow('Image', frame)
        cv2.waitKey(1)
